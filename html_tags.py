head_top = """<!DOCTYPE html>
<html>
  <head>
    <title>Title</title>
    <meta charset="utf-8">
"""

style = """
<style>
    /* Two-column layouts */
    .left-column  { width: 49%; float: left; }
    .right-column { width: 49%; float: right; }

    .left-column-33  { width: 33%; float: left; }
    .right-column-66 { width: 66%; float: right; }

    .left-column-66  { width: 66%; float: left; }
    .right-column-33 { width: 33%; float: right; }

    .right-column ~ p { clear: both; }
    .right-column ~ ul { clear: both; }

    div.my-header {
      background-color: #F77A00;
      position: fixed;
      top: 0px;
      left: 0px;
      height: 30px;
      width: 100%;
      text-align: center;
    }

    div.my-footer {
        border-top: 1px solid #ccc;
      text-align: center;
      position: fixed;
      bottom: 0px;
      left: 0px;
      height: 30px;
      width: 100%;
    }

</style>
"""

import_libs = """
<!-- CSS only -->
<link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta2/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-BmbxuPwQa2lc/FVzBcNJ7UAyJxM6wuqIj61tLrc4wSX0szH/Ev+nYRRuWlolflfl" crossorigin="anonymous">

<!-- JavaScript Bundle with Popper -->
<script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta2/dist/js/bootstrap.bundle.min.js" integrity="sha384-b5kHyXgcpbZJO/tY9Ul7kGkf1S0CWuKcCD38l8YkeH8z8QjE0GmW1gYU5S9FOnJ0" crossorigin="anonymous"></script>

"""

head_bottom ="</head>"

body_top = """
<body>
"""


scripts = """
    <script src="https://remarkjs.com/downloads/remark-latest.min.js"></script>
"""
body_bottom = """   
    <script>
      var slideshow = remark.create();
    </script>
  </body>
</html>
"""

