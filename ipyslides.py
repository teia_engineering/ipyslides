import base64
import html_tags
import internal_libs
import numpy as np
import time
import pandas as pd
from urllib.parse import unquote
import codecs
import json
from matplotlib import pyplot as plt
import ipykernel.zmqshell as zmq
from IPython.display import display, HTML

def background_image(img_name):  
    with open(img_name, "rb") as img_file:
        b64_str = base64.b64encode(img_file.read())

    return 'background-image: url("data:image/png;base64,'+b64_str.decode('utf-8')+'")'

def background_color(color):
    return 'background-color: '+color
# from IPython.core.display import HTML
# HTML(background_imaage("./bg_img.jpeg"))

def header(commands, idx, continuous="false"):
    temp = {}
    if continuous == "true":
        temp["continuous"] = "layout: true<b/>"
    text = ""
    color = ""
    if '"' in commands[idx+1]:
        text = text+commands[idx+1]
        for idx2,command in enumerate(commands[idx+2:]):
            
            if '"' in command:
                text = text+" "+command
                color = commands[idx2+idx+3]
                break
                
            else:
                text = text+" "+command
                
    else:
        text = commands[idx+1]
        color = commands[idx+2]
        
    text = text.replace('"',"")
    
    temp["other"] = '<div class="my-header", style="background-color:'+("transparent" if color == 'none' else color)+'">'+text+'</div>'
    
    return temp
    
def footer(commands, idx, continuous="false"):
    temp = {}
    if continuous == "true":
        temp["continuous"] = "layout: true<b/>"
        
    text = ""
    color = ""
    if '"' in commands[idx+1]:
        text = text+commands[idx+1]
        for idx2,command in enumerate(commands[idx+2:]):
            
            if '"' in command:
                text = text+" "+command
                color = commands[idx2+idx+3]
                break
                
            else:
                text = text+" "+command
    else:
        text = commands[idx+1]
        color = commands[idx+2]
        
    text = text.replace('"',"")
    
        
    temp["other"] = '<div class="my-footer", style="background-color:'+("transparent" if color == 'none' else color)+'">'+text+'</div>'
    
    return temp

def alignment(commands, scope, return_data):
    commands = commands.split(",")
    
    if scope == "global":
        return_data['class'] = return_data['class']+commands
        
    else:
        if commands[0] in return_data['line'].keys():
            return_data['line'][commands[0]].append(scope)
            
        else:
            return_data['line'][commands[0]] = [int(x) for x in scope.split(",")]
            
    return return_data

# featuares for code blocks
def feature_check_loop(cell, commands, loop_location):
    temp = {"styles":[],"others":[]}
    temp_classes = {'class':[], 'line':{}}
    for idx, val in enumerate(commands):
        if val == "code" and str(loop_location) == commands[idx+1]:
            temp['others'].append("\n```python\n"+"".join(cell["source"][1:])+"\n```\n")
                
    return temp

def feature_alignment_line(line, commands, loop_location, return_data):
    temp_classes = {'class':[], 'line':{}}
    add_line = True
    for idx, val in enumerate(commands):
        if val == "alignment":
            temp_classes = alignment(commands[idx+1], commands[idx+2], temp_classes)
            if len(temp_classes["line"].keys()) > 0 and loop_location in temp_classes["line"][commands[idx+1]]:
                return_data.append([commands[idx+1], line])
                add_line = False
    if add_line:
        return_data.append([line])
    return return_data

def feature_check_end(cell, commands):
    temp =  {"styles":[],"others":[]}
    temp_classes = {'class':[], 'line':{}}
    for idx, val in enumerate(commands):
        if (len(cell['outputs']) == 0 and val == "code" and '0' == commands[idx+1]) or ( val == "code" and len(cell['outputs'])  < int(commands[idx+1])):
            temp['others'].append("\n```python\n"+"".join(cell["source"][1:])+"\n```\n")

        elif val == "background-image":
            temp['styles'].append(background_image(commands[idx+1]))
            
        elif val == "background-color":
            temp['styles'].append(background_color(commands[idx+1]))
            
        elif val == "header":
            temp['others'].append(header(commands, idx)['other'])
            
        elif val == "footer":
            temp['others'].append(footer(commands, idx)['other'])
            
        elif val == "alignment":
            temp_classes = alignment(commands[idx+1], commands[idx+2], temp_classes)
    
    return temp, temp_classes

# feature for markdown blocks
def md_commands(commands):
    temp =  {"styles":[],"others":[]}
    temp_classes = {'class':[], 'line':{}}
    for idx, val in enumerate(commands):
        if val == "background-image":
            temp['styles'].append(background_image(commands[idx+1]))
            
        elif val == "background-color":
            temp['styles'].append(background_color(commands[idx+1]))
            
        elif val == "header":
            temp['others'].append(header(commands, idx)['other'])
            
        elif val == "footer":
            temp['others'].append(footer(commands, idx)['other'])
            
        elif val == "alignment":
            temp_classes = alignment(commands[idx+1], commands[idx+2], temp_classes)
    
    return temp, temp_classes

def create_markdown(file_name):
    # Need to save notebook changes, just making sure in case the
    # user didnt and we dont have the latest file in memory.
    k_magics = zmq.KernelMagics()
    k_magics.autosave(1)

    # Wait two second for file to safe before doing anything else.
    time.sleep(2)
    k_magics.autosave(120)

    # reading file after been saved
    check_file = True
    while check_file:
        try:
            f=codecs.open(unquote(file_name+".ipynb"), 'r')
            file = json.loads(f.read())
            check_file = False
        except Exception as exp:
            print("Error on loading file: " + expe.args[1])
            return expe.args[1]
        
    print("Read File")

    # getting markdowns/code cells outputs.
    markdown = []
    for cell in file['cells']:
        # Checking to see if cell is markdown already
        if cell['cell_type'] == 'markdown' and  len(cell['source']) > 0:
            # Does it have the library command
            if "<!-- ipysl" in cell['source'][0] or "<!--ipysl" in cell['source'][0]:
                temp,temp_classes = md_commands(cell['source'][0].split(" "))
                temp_return = []
                for idx,row in enumerate(cell['source'][1:]):
                    temp_return = feature_alignment_line(row, cell['source'][0].split(" "), idx, temp_return)

                if len(temp_return) > 0:
                    data = []
                    t_alignment = {}
                    for row in temp_return:
                        if len(row) == 1:
                            for align in t_alignment.keys():
                                data.append("."+align+"["+"".join(t_alignment[align])+"]\n")
                            t_alignment = {}
                            data.append(row[0]+"\n")

                        else:
                            if row[0] in t_alignment:
                                t_alignment[row[0]].append(row[1])
                            else:
                                t_alignment[row[0]] = [row[1]]
                    if len(t_alignment) > 0:
                        for align in t_alignment.keys():
                            data.append("."+align+"["+"\n".join(t_alignment[align])+"]\n")
                        t_alignment = {}

                else:
                    data = ["\n".join(cell['source'][1:])]

                data.append("\n".join(temp["others"]))
                for feat in temp['styles']:
                    data.insert(0,feat+"\n")

                if len(temp_classes['class']) > 0:
                    data.insert(0,"class: "+",".join(temp_classes['class'])+"\n\n")
                markdown.append("".join(data))

        elif cell['cell_type'] == 'code':
            if len(cell['source']) > 0 and "#" in cell['source'][0] and "ipysl" in cell['source'][0]:
                # splitting the command to get what we want.
                commands = cell['source'][0].split("ipysl")[1].strip().replace('\n','').split(" ")
                temp = []
                temp_return = []
                row = 0
                for idx, output in enumerate(cell['outputs']):
                    if "text" in output:
                        for r in output["text"]:
                            feature_vals = feature_check_loop(cell, commands, row)
                            
                            temp_return = feature_alignment_line("\n".join(feature_vals['others']), commands, idx, temp_return)

                            temp_return = feature_alignment_line(r, commands, row, temp_return)

                            temp.append("\n".join(feature_vals['others']))

                            row = row+1

                    elif "data" in output:
                        feature_vals = feature_check_loop(cell, commands, row)

                        temp_return = feature_alignment_line("\n".join(feature_vals['others']), commands, row, temp_return)

                        temp.append("\n".join(feature_vals['others']))
                        if "image/png" in output["data"]:
                            image = '<img src="data:image/png;base64,'+output["data"]['image/png']+'">'
                            temp_return = feature_alignment_line(image, commands, row, temp_return)
                            temp.append(image)
                        row = row+1
                
                if len(temp_return) > 0:
                    temp = []
                    t_alignment = {}
                    for row in temp_return:
                        if len(row) == 1:
                            for align in t_alignment.keys():
                                temp.append("."+align+"["+"\n".join(t_alignment[align])+"]\n")
                            t_alignment = {}
                            temp.append(row[0]+"\n")

                        else:
                            if row[0] in t_alignment:
                                t_alignment[row[0]].append(row[1])
                            else:
                                t_alignment[row[0]] = [row[1]]
                    if len(t_alignment) > 0:
                        for align in t_alignment.keys():
                            temp.append("."+align+"["+"\n".join(t_alignment[align])+"]\n")
                        t_alignment = {}

                feature_vals,temp_classes = feature_check_end(cell, commands)
                temp.append("\n".join(feature_vals["others"]))
                for feat in feature_vals['styles']:
                    temp.insert(0,feat+"\n\n")

                if len(temp_classes['class']) > 0:
                    temp.insert(0,"class: "+",".join(temp_classes['class'])+"\n\n")

                markdown.append("".join(temp))
    return markdown

def generate_html_doc(filename, js_css="internal", css=[], javascript=[], remark_js=[]):
    """
        This function will generate the HTML code that will be stored into the index.html
        file. The function allows the user to select between a mix match of ways to get 
        the jascript and css files.
        
        Params:
            js_css - Tell the function which way to get the CSS/Javascript files. The main options are internal, external or personal. internal(or offline) uses the libraries provided in the package. external uses provided urls in the package for the libraries. external will use urls passed in the other parameters of this function. You can mix them using the an underscore between them. Ex. external_personal. 
            css - List containing urls to external CSS files.
            javascript - List containing urls to external JS files
    """
    markdown = create_markdown(filename)
    if markdown == "No such file or directory":
        return "error"
    markdowns = "\n---\n".join(list(filter(None, markdown)))+"\n"
    textarea = '<textarea id="source">'+markdowns+'</textarea>'
    
    # Which libraries to use.
    temp = []
    temp_remark_js = []
    if "internal" in js_css:
        temp.append("<script>"+internal_libs.bootstrap_popper+"</script>")
        temp.append("<script>"+internal_libs.bootstrap_javascript+"</script>")
        temp.append("<script>"+internal_libs.bootstrap_jquery+"</script>")
        temp.append("<style>"+internal_libs.bootstrap_css+"</style>")
        temp_remark_js.append("<script type='text/javascript'>"+internal_libs.remark_js+"</script>")
        
    if "external" in js_css:
        for url in internal_libs.bootstrap_urls_css:
            temp.append('<link href="'+url+'" rel="stylesheet">')
            
        for url in internal_libs.bootstrap_urls_js:
            temp.append('<script src="'+url+'"></script>')
        
        for url in internal_libs.remark_urls_js:
            temp_remark_js.append('<script src="'+url+'"></script>')
    
    if "personal" in js_css:
        for url in css:
            temp.append('<link href="'+url+'" rel="stylesheet">')
            
        for url in javascript:
            temp.append('<script src="'+url+'"></script>')
            
        if len(remark_js) > 0:
            temp_remark_js = []
            
            for url in internal_libs.remark_urls_js:
                temp_remark_js.append('<script src="'+url+'"></script>')
            
    html_tags.import_libs = "\n".join(temp)
    html_tags.scripts = "\n".join(temp_remark_js)
        
    html_doc = (html_tags.head_top +
        html_tags.import_libs +
        html_tags.style +
        html_tags.head_bottom +
        html_tags.body_top + 
        textarea +
        html_tags.scripts + 
        html_tags.body_bottom
    )
    
    return html_doc

def create_file(notebook_name, file_name="index.html", js_css="internal", css=[], javascript=[], remark_js=[]):
    html_doc = generate_html_doc(notebook_name, js_css, css, javascript, remark_js)
    
    if html_doc == "error":
        return "Error"
    
    text_file = open(file_name, "w")
    text_file.write(html_doc)
    text_file.close()

    display(HTML('<a href="./'+file_name+'" target="_blank">Go see your slides</a>'))
    